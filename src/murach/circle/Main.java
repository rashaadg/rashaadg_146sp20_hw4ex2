package murach.circle;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to the Circle Calculator");
        System.out.println();

        Scanner sc = new Scanner(System.in);
        
        String choice = "y";
        
        while (choice.equals("y")) {
            // get input from user
            /*
             * HW4 Ex 2, step 7: Modify the code that gets the radius from
             * the user so it prompts the user to enter one or more radiuses
             * (or "radii," if you prefer) on the same line, with each radius
             * seperated by a space. Store this line in a String variable
             * called line.
             */
            String line = sc.nextLine();
            
            String line1 = sc.next();
            String line2 = sc.next();
            System.out.println("Enter one or more radius values, seperated by spaces:  " + line1 + line2);
            
            /*
             * HW4 Ex 2, step 8: Modify the code so it parses the line of text that the user enters to 
             * get a 1-D array of String objects, with one String object for each entry 
             * (you can call the String array entries). NOTE: We haven't studied
             * String manipulations in Java yet, so to handle the parsing of the 
             * "String of radii" into separate Strings, you can use the trim and split 
             * instance methods of the String class like this:
             */
            String entries[] = line.split(" "); // "tokenizies" or "parses" the String of radii
                                                // (with radius values seperated by spaces)
                                                // into an array of String objects ("tokens")
            
            /*
             * HW4 Ex 2, step 9: Declare and create a new 1-D array of Circle object 
             * references (which you might call circles), such that the length of 
             * circles will have the same length as the entries array created above.
             */
            int[] circles = new int[1]; 
            for (int i = 0; i < circles.length; i++) { 
                circles[i] = i;
                System.out.println( "i = " + i + ", " + "circles[i] = " + circles[i] );
            } // end for
            
            /*
             * HW4 Ex 2, step 10: Write a counter-controlled for loop that iterates 
             * through each element in the entries array. For each element in this 
             * array, convert the String referred to by the current entries element into a
             * double, and store this double in a variable called radius. Use this value of 
             * radius as the argument to the "one-arg" Circle constructor, and store a 
             * reference to your newly-instantiated Circle object in the current element
             * of the circles array. 
             */
             for ( double radius : circles )
            {
                System.out.println( circles );
            } // end enhanced for loop
            
            /*
             * HW4 Ex 2, step 11: Create another loop that iterates through the 
             * array of Circle objects and displays the data for each Circle 
             * object on the console. This data should include the radius, 
             * area, circumference, and diameter, each displayed on its own line 
             */
            String message =
                "Radius:        " + circle.getRadius() + "\n" +    
                "Area:          " + circle.getArea() + "\n" +
                "Circumference: " + circle.getCircumference() + "\n" +
                "Diameter:      " + circle.getDiameter() + "\n";
            System.out.println(message);
             
            // create the Circle object
            Circle circle = new Circle(line);
            
            // format and display output
            String message =
                "Radius:        " + circle.getRadius() + "\n" +
                "Area:          " + circle.getArea() + "\n" +
                "Circumference: " + circle.getCircumference() + "\n" +
                "Diameter:      " + circle.getDiameter() + "\n";
            System.out.println(message);

            // see if the user wants to continue5
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        }
        System.out.println("Bye!");
        sc.close();
    }   
} // end main method