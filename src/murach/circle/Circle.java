package murach.circle;

public class Circle {
    private double radius;
    
    public Circle() {
        radius = 0;
    }
    
    public Circle(double radius) {
        this.radius = radius;
    }

    Circle(String line) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    public double getCircumference() {
        return 2 * 3.14159 * radius;
    }
    
    public double getArea() {
        return 3.14159 * radius * radius;
    }
    
    public double getDiameter() {
        return radius * 2;
    }
}
